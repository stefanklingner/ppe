/**
 * Created by Stefan on 10.04.16.
 */

var assert = require('chai').assert;

var users = require('../public/javascripts/users');

describe('user_test', function() {

    describe('#class creation', function () {

        it('should not be null', function () {
            var u = new users();
            assert.isNotNull(u, "u is null");
        });

    });

    describe('#user tests', function() {

        it('should find one', function () {

            var u = new users();
            u.findOne({username: 'testuser'}, function(err, user) {
                assert.isNotNull(user);
            });

        });

        it('user name should be testuser', function () {

            var u = new users();
            u.findOne({username: 'testuser'}, function(err, user) {
                assert.isTrue(user.name === 'testuser');
            });

        });

        it('user should not be found', function () {

            var u = new users();
            u.findOne({username: 'xy'}, function(err, user) {
                assert.isTrue(user ===  null);
            });

        });

        it('password should be valid', function () {

            var u = new users();
            u.findOne({username: 'testuser'}, function(err, user) {
                assert.isTrue(user.name === 'testuser');
                assert.isTrue(user.validPassword(user.password));
            });

        });
        
        it('user should be found by id', function () {

            var u = new users();
            u.findById(1, function(err, user) {
                assert.isFalse(user === null);
                assert.isTrue(user.id === 1);
                assert.isTrue(user.name === 'testuser');
            });

        });

    });

});
