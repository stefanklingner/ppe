/**
 * Created by Stefan on 10.04.16.
 */

var userList = [

    { id: 1, name: 'testuser', password: 'test123' },
    { id: 2, name: 'testuser2', password: 'test456' },
    { id: 3, name: 'testuser3', password: 'test789' }

];

function Users() {

    //private
    var self = this;

    var validPassword = function(password) {

        return password === this.password;

    };

    self.findOne = function(data, callback) {

        if (data.username != null) {

            var result = null;
            for (i = 0; i < userList.length; i++) {
                if (userList[i].name === data.username) {
                    result = userList[i];

                    result.validPassword = validPassword;

                    break;
                }
            }

            callback(null, result);

        } else {

            callback(null, null);
        }

    };

    self.findById = function(id, callback) {
        var result = null;
        for (i = 0; i < userList.length; i++) {
            if (userList[i].id === id) {
                result = userList[i];
                break;
            }
        }
    };

};

module.exports = Users;