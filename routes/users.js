var express = require('express');
var router = express.Router();

var passport = require('passport');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('login', { title: 'Login' });
});

router.post('/login',
    passport.authenticate('local', { successRedirect: '/',
      failureRedirect: '/user/login',
      failureFlash: true })
);

router.get('/register',function(req, res, next) {
  res.render('register');
});

module.exports = router;
